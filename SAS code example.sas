/* EXAMPLE SAS CODE for workshop on 
* 	SAS Studio integration with Git & GitLAB
*/

/* First chunk of code here: */
	/* load CARS dataset */
	data work.cars;
		set SASHELP.CARS;
	run;

	/* calculate the number of unique models */
	proc sql;
		title 'Number of unique models BY MAKE in the CARS dataset';
		create table work.model_count as
			select Make
				,  count(unique Model) as 'Number of models'n
			from work.cars
			group by Make;
	quit;